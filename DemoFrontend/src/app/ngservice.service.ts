import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { User } from './user';
import { Account } from './account';

@Injectable({
  providedIn: 'root'
})
export class NgserviceService {

  constructor(private _http:HttpClient) { }

  fetchUserList():Observable<any>{
    return this._http.get<any>("http://localhost:8080/allUsers");
  }

  addNewUser(user:User):Observable<any>{
    return this._http.post<any>("http://localhost:8080/addUser",user);
  }

  loginCheck(user1:User):Observable<any>{
    return this._http.post<any>("http://localhost:8080/login",user1);
  }

  // addNewAccount(account:Account):Observable<any>{
  //   return this._http.post<any>("http://localhost:8080/addAccount",account);
  // }

  addNewAccount1(account:Account,id:number):Observable<any>{
    console.log(account);
    console.log("ID "+id);
    return this._http.post<any>("http://localhost:8080/addAccount/"+id,account);
  }
  
  editUserDetails(user:User):Observable<any>{
    return this._http.post<any>("http://localhost:8080/updateUser",user);
  }

  editAccountDetails(acc:Account,id:number):Observable<any>{
    return this._http.post<any>("http://localhost:8080/updateAcc/"+id,acc);
  }

  deleteUserDetails(id:number):Observable<any>{
    return this._http.delete<any>("http://localhost:8080/deleteUser/"+id);
  }

  
  deleteUserAccounts(id:number):Observable<any>{
    return this._http.delete<any>("http://localhost:8080/deleteUserAccounts/"+id);
  }

  fetchUserByID(id:number):Observable<any>{
    return this._http.get<any>("http://localhost:8080/getUserByID/"+id);
  }

  getAccount(id:number):Observable<any>{
    return this._http.get<any>("http://localhost:8080/getAccByUserID/"+id);
  }

  fetchAccountByID(id:number):Observable<any>{
    return this._http.get<any>("http://localhost:8080/getAccount/"+id);
  }
  
}
