import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Toast, ToastrService } from 'ngx-toastr';
import { Session } from '../session';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private _route:Router, private toastr:ToastrService) {
    this.currentUser = this.getCurrentName();
   }

  selectId!:number;
  selectName!:string;
  condn : boolean=false;
  condn1 : boolean=true;
  getUserData:any;
  currentUser!:string;

  @Input()
  session:any= Session;
  

  ngOnInit(): void {
    this.session = new Session();
     this.getUserData =JSON.parse(localStorage.getItem('userDetails')!);
     if(this.getUserData != null){
    //  console.log(this.getUserData.user_ID);
    //  console.log(this.getUserData.name);
     this.selectName = this.getUserData.name;
     }
   
  }

   getCurrentName():string{
    this.getUserData =JSON.parse(localStorage.getItem('userDetails')!);
    let myArray : string[];
    let showcase !: string ;
     if(this.getUserData != null){
    //  console.log(this.getUserData.user_ID);
    //  console.log(this.getUserData.name);
     this.selectName = this.getUserData.name;
     myArray = this.selectName.split(" ");
     showcase = myArray[0];
     
   }
   return showcase;

   
//  this.user =JSON.parse(localStorage.getItem('userDetails')!);
//     let myArray : string[];
//     let showcase !: string ;
//      if(this.user != null){
//     //  console.log(this.getUserData.user_ID);
//     //  console.log(this.getUserData.name);
//     this.showName = this.user.name;
//     myArray = this.showName.split(" ");
//     showcase = myArray[0];
// }
// //myArray = text.split(" ");
// return showcase;


  }

  logout(){
    this.toastr.show("Successfully logged out", "Success");
    this.session.logout();  
    console.log("localstorage is cleared")
    window.localStorage.clear();
    this._route.navigate(['/login'])
  }

}
