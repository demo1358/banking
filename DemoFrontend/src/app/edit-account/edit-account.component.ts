import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Account } from '../account';
import { NgserviceService } from '../ngservice.service';

@Component({
  selector: 'app-edit-account',
  templateUrl: './edit-account.component.html',
  styleUrls: ['./edit-account.component.css']
})
export class EditAccountComponent implements OnInit {
  selectId:any;

  accData!:number;
  
  account = new Account();

  constructor(private _route:Router,private _service:NgserviceService, private toastr:ToastrService,private _activatedRoute:ActivatedRoute) { }

  ngOnInit(): void {
    this._activatedRoute.paramMap.subscribe(
      params=>{
        this.selectId=(params.get('id'));
        console.log(this.selectId);
      }
     
    )
    this._service.fetchAccountByID(this.selectId).subscribe(
      data=>{
        console.log("Data received");
        this.account=data;
        console.log(data);
      },
      error=>{
        console.log("exception occured");
      }
    )
    console.log(this.selectId);
  }

  BackEdit(){
    let getUserData =JSON.parse(localStorage.getItem('userDetails')!);
    console.log(getUserData.user_ID);
    
    console.log(this.selectId);
    this._route.navigate(['/viewAccount',getUserData.user_ID]);
    //this.router.navigate(['/viewAccount',id]);
  }

  editAccountFormSubmit(editAccountForm:any){
    let getUserData =JSON.parse(localStorage.getItem('userDetails')!);
    console.log(getUserData.user_ID);
    if(getUserData.user_ID!=null){
    let id = getUserData.user_ID
    console.log(this.account);
    this._service.editAccountDetails(this.account,id).subscribe(
      data=>{console.log("data added successfully");console.log(data);
      if(data==0){
        this.toastr.error("Not authorized","Failure")
       }else{
        this.toastr.success("Account Details updated","Success")
       }
      
    },
      error=>{console.log("error occured")}  ,
       
    )
    
    
  
    this._route.navigate(['/getUsers']);
   
  }
}

}
