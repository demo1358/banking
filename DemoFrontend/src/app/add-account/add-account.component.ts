import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Account } from '../account';
import { NgserviceService } from '../ngservice.service';
import { User } from '../user';

@Component({
  selector: 'app-add-account',
  templateUrl: './add-account.component.html',
  styleUrls: ['./add-account.component.css']
})
export class AddAccountComponent implements OnInit {
  account = new Account();

  constructor(private _route:Router,private _service:NgserviceService,private _activatedRoute:ActivatedRoute, private toastr:ToastrService) { }
  
  //getUserData:any;
  selectId!:number ;
  

  ngOnInit(): void {
    let getUserData =JSON.parse(localStorage.getItem('userDetails')!);
    //console.log(getUserData.user_ID);
    if(getUserData!=null){
    this.selectId = getUserData.user_ID
    }
    console.log((getUserData));

  
  }

  addAccountFormSubmit(addAccountForm:any){
    this._service.addNewAccount1(this.account,this.selectId).subscribe(
      data=>{console.log("data added successfully");
      if(data!=null){
        this.toastr.success("Account added successfully","Success");
      }else{
        // console.log("invalid credentials");
        this.toastr.error("Account not added","Failure");
        this._route.navigate(['/addAccount']);
      }
    },
      error=>console.log("error occured"),
    
    )
    
    this.toastr.success("Account added successfully","Success");
    console.log(addAccountForm);
    this._route.navigate(['/getUsers']);
  }

}
