import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { NgserviceService } from '../ngservice.service';
import { User } from '../user';

@Component({
  selector: 'app-edit-user-details',
  templateUrl: './edit-user-details.component.html',
  styleUrls: ['./edit-user-details.component.css']
})
export class EditUserDetailsComponent implements OnInit {
  user= new User();

  constructor(private _route:Router,private _service:NgserviceService,private _activatedRoute:ActivatedRoute, private toastr:ToastrService) { }

  selectId: any;

  ngOnInit(): void {
      // this._activatedRoute.paramMap.subscribe(
      // params=>{
      //     this.selectId=parseInt(params.get('id'));
      // }  
      // )
      this._activatedRoute.paramMap.subscribe(
        params=>{
          this.selectId=(params.get('id'));
        }
       
      )
      this._service.fetchUserByID(this.selectId).subscribe(
        data=>{
          console.log("Data received");
          this.user=data;
        },
        error=>{
          console.log("exception occured");
        }
      )
      console.log(this.selectId);
  }

  editUserFormSubmit(){
    console.log(this.user);
    this._service.editUserDetails(this.user).subscribe(
      data=>{console.log("data edited successfully");
        if(data!=null){
          this.toastr.success("User Details updated","Success");
          this._route.navigate(['/getUsers']);
        }else{
          this.toastr.error("User Details not updated","Failure");
          this._route.navigate(['/getUsers']);
        }
      },
      error=>console.log("error occured"),
      
    )
 
    
  
    this._route.navigate(['/getUsers']);
   
  }
  

}
