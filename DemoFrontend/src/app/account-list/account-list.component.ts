import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Account } from '../account';
import { NgserviceService } from '../ngservice.service';

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.css']
})
export class AccountListComponent implements OnInit {

  accountData:any=[];
  constructor(private _route:Router,private _service:NgserviceService,private _activatedRoute:ActivatedRoute) { }

  selectId: any;

  ngOnInit(): void {
  
      this._activatedRoute.paramMap.subscribe(
        params=>{
          this.selectId=(params.get('id'));
        }
       
      )
      this._service.getAccount(this.selectId).subscribe(
        data=>{
          localStorage.setItem("accountData",JSON.stringify(data));
          console.log(data);
          console.log("Data received");
          this.accountData=data;
        },
        error=>{
          console.log("exception occured");
        }
      )
      console.log(this.selectId);
  }

  editAccount(id:number){
    this._route.navigate(['/editAcc',id]);
  }

}
