import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService:AuthService,private route:Router){

  }

  canActivate() {
    if(this.authService.isUserLoggedIn()){
      return true;
    }else{
      // this.route.navigate(["/login"]);
      //window.alert("permission denied!! Please Sign in!!")
      return this.route.createUrlTree(['/login']);
    }
  }
 
  
}
