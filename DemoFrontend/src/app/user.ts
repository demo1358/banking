export class User{
    user_ID!: number;
    name!: string;
    email_ID!: string;
    mobile_number!: string;
    secondary_Mobile!: string;
    sampleDate!: Date;
    gender!: string;
    password!: string;
}