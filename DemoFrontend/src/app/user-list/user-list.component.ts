import { Component, OnInit } from '@angular/core';
import { NgserviceService } from '../ngservice.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  userData:any=[];

  constructor(private router: Router,private _service: NgserviceService, private toastr:ToastrService) {
    let userData = this._service.fetchUserList().subscribe(data=>{
     this.userData=data;
    });
    console.log(userData);
   
    
   }

  ngOnInit(){
    this._service.fetchUserList().subscribe(
      data=>console.log("response received"),
      error=>console.log("exception occured")
    )
   
  }

  btnClickEdit=() =>{
    this.router.navigate(['/editUser']);
  }

  goToEditUser(id:number){
    console.log(id);
    this.router.navigate(['/editUser',id]);
  }

  goToDeleteUser(id:number){

    if(confirm("Are you sure you want to Permanently delete")) {
      console.log("Implement delete functionality here");
    
    console.log(id);
    this._service.deleteUserDetails(id).subscribe(
      data=>{console.log("response received");
      this._service.deleteUserAccounts(id).subscribe(     
        data=>{console.log("response received");
        this.reloadComponent();
        },
        error=>console.log("exception occured")
      )
      console.log(data);
        if(data!=null){
          this.toastr.success("User details successfully deleted","Success"); 
         
        }else{
          this.toastr.error("User details not deleted","Failure");
          
        }
        this.ngOnInit();
        
      },
       error=>console.log("exception occured")
    )
  
    
    //window.location.reload();
    this.ngOnInit();
    
  }

}

  reloadComponent() {
        let currentUrl = this.router.url;
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([currentUrl]);
    }
    
  goToAddAccount(id:number){
    console.log(id);
    this.router.navigate(['/addAccount',id]);
  }



  goToViewAccount(id:number){
    console.log(id);
    this.router.navigate(['/viewAccount',id]);
  }
    //this.router.navigate(['/getUsers']);
  }


