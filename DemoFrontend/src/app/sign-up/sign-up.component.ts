import { Component, OnInit } from '@angular/core';
import { NgserviceService } from '../ngservice.service';
import { NgForm } from '@angular/forms';
import { User } from '../user';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  user = new User();



  constructor(private _service:NgserviceService, private router:Router, private toastr:ToastrService) { }

  ngOnInit(): void {
  }

  hide : boolean = true;

myFunction() {
  this.hide = !this.hide;
}

getCurrentDate(): Date{

  let today = new Date();
  let yyyy1 = today.getFullYear();

  let mm1: any = today.getMonth() + 1; // Months start at 0!let dd = today.getDate();
  let dd1: any = today.getDate();

  const formattedToday = yyyy1 + '-' + mm1 + '-' + dd1;
  console.log(today);
  return today;
}

  addUserFormSubmit(addUserForm:any){
    console.log(this.getCurrentDate());
    this._service.addNewUser(this.user).subscribe(     
      data=>{console.log("data added successfully")
      if(data!=null){
        this.toastr.success("User Successfully Added","Success");
        this.router.navigate(['/login']);
      }else{
        this.toastr.error("User Not Added","Failure");
        this.router.navigate(['/register']);
      }
      },
      error=>console.log("error occured"),
     
    )

    

  }



}
