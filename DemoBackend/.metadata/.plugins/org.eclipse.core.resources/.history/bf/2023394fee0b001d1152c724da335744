package com.example.demo.entities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int User_ID;

	@Pattern(regexp = "^[a-zA-Z0-9.\\-\\/+=@_ ]*$", message = "Wild characters in Name are not Allowed")
	@Column
	@NotNull(message = "Name is Required!!")
	@Size(max = 40, message = "Name must be of max 40 char")
	private String Name;

	@Email(message = "Email Address is Invalid!!")
	@NotBlank(message = "Invalid Email Address!!")
	@Size(max = 100, message = "Email must be of max 100 char")
	@Column(unique = true)
	private String Email_ID;

	@Column
	@Pattern(regexp = "^[0-9]{10}$", message = "Invalid Mobile Number!!")
	@Size(max = 10, min = 10, message = "Mobile number must be of 10 digits!!")
	private String Mobile_number;

	@Column
	// @Pattern(regexp = "^[0-9]{10}$", message = "Invalid Mobile Number!!")
	// @Size(max=10,min=10,message = "Mobile number must be of 10 digits!!")
	private String Secondary_Mobile;

	// @NotEmpty
	// @JsonFormat(pattern="yyyy-MM-dd", shape=Shape.STRING)
//	@Column
//	private String DOB;

	// @NotEmpty
//	@Column
//	private char Gender;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "gender")
	private GenderEnum Gender;

	
	public enum GenderEnum {
	    M, F
	}
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	//@Temporal(TemporalType.DATE)
	@Column
	private Date sampleDate;

	@Column
	private String Password;

//	@OneToMany(targetEntity = Account.class, cascade = CascadeType.ALL)
//	@JoinColumn(referencedColumnName = "User_ID")
//	private List<Account> accounts;

	@OneToMany(mappedBy = "User_ID", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Account> accounts;

	// SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(String name, String email_ID, String mobile_number, String secondary_Mobile, GenderEnum gender,
			String sampleDate, String password) throws ParseException {

		Name = name;
		Email_ID = email_ID;
		Mobile_number = mobile_number;
		Secondary_Mobile = secondary_Mobile; // yyyy-mm-dd
		Gender = gender;
		this.sampleDate = new Date(new SimpleDateFormat("dd MMM yyyy").parse(sampleDate).getDate());
		this.Password = password;

	}
	

	public User(int user_ID, String name, String email_ID, String mobile_number, String secondary_Mobile, GenderEnum gender,
			String sampleDate, String password) throws ParseException {
		super();
		User_ID = user_ID;
		Name = name;
		Email_ID = email_ID;
		Mobile_number = mobile_number;
		Secondary_Mobile = secondary_Mobile;
		Gender = gender;
		this.sampleDate = new Date(new SimpleDateFormat("dd MMM yyyy").parse(sampleDate).getDate());
		this.Password = password;
	}

	public int getUser_ID() {
		return User_ID;
	}

	public void setUser_ID(int user_ID) {
		User_ID = user_ID;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getEmail_ID() {
		return Email_ID;
	}

	public void setEmail_ID(String email_ID) {
		Email_ID = email_ID;
	}

	public String getMobile_number() {
		return Mobile_number;
	}

	public void setMobile_number(String mobile_number) {
		Mobile_number = mobile_number;
	}

	public String getSecondary_Mobile() {
		return Secondary_Mobile;
	}

	public void setSecondary_Mobile(String secondary_Mobile) {
		Secondary_Mobile = secondary_Mobile;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

//	public char getGender() {
//		return Gender;
//	}
//
//	public void setGender(char gender) {
//		Gender = gender;
//	}
	
	

	public Date getSampleDate() {
		return sampleDate;
	}

//	public void setSampleDate(String sampleDate) throws ParseException {
//		this.sampleDate = new Date(new SimpleDateFormat("dd MMM yyyy").parse(sampleDate).getDate());
//	}

	public GenderEnum getGender() {
		return Gender;
	}

	public void setGender(GenderEnum gender) {
		Gender = gender;
	}

	@Override
	public String toString() {
		return "User [User_ID=" + User_ID + ", Name=" + Name + ", Email_ID=" + Email_ID + ", Mobile_number="
				+ Mobile_number + ", Secondary_Mobile=" + Secondary_Mobile + ", Gender=" + Gender + ", sampleDate="
				+ sampleDate + "]";
	}

//	public void setSampleDate(Date sampleDate) {
//		this.sampleDate = sampleDate;
//	}

//	String pattern = "yyyy-MM-dd";
//	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//
//	Date date = simpleDateFormat.parse("2018-09-09");

}
