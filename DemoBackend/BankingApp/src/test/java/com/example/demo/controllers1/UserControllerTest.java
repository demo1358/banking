package com.example.demo.controllers1;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.example.demo.controllers.UserController;
import com.example.demo.entities.User;
import com.example.demo.entities.User.GenderEnum;
import com.example.demo.repository.UserRepository;

@SpringBootTest
public class UserControllerTest {

	@MockBean
	UserRepository urepo;

	@MockBean
	User user;

	@Autowired
	UserController uc;

	private User userforTest;
	private List<User> list;
	private ResponseEntity<User> responseEntity;
	

	public UserControllerTest() throws ParseException {

		userforTest = new User(1, "Lokesh", "Lokesh@gmail.com", "1212121212", "2121212121", GenderEnum.M, "02 JAN 2020",
				"lokesh");

		list = new ArrayList<>();
		list.add(userforTest);

		responseEntity = ResponseEntity.status(HttpStatus.OK).body(userforTest);
	}

	@Test
	void testGetAllUsersList() throws ParseException {
		
		ResponseEntity<List<User>> res= new ResponseEntity<List<User>>(list, HttpStatus.OK);

		when(urepo.findAll()).thenReturn(Stream.of(
				new User(1, "Lokesh", "Lokesh@gmail.com", "1212121212", "2121212121", GenderEnum.M, "02 JAN 2020",
						"lokesh"))
				.collect(Collectors.toList()));

		assertThat(responseEntity).isNotNull();
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(list.size()).isEqualTo(1);
		// assertThat(responseEntity).isEqualTo(uc.getAllUsersList());
		assertEquals(res.getBody().get(0).getName(), uc.fetch().getBody().get(0).getName());
		//assertThat(uc.fetch().getBody()).isEqualTo(res.ge);
	}

	@Test
	void testGetUserById() {
		when(urepo.findById(1)).thenReturn(Optional.of(userforTest));
		assertEquals(responseEntity, uc.getUserById(1));
		// assertThat((User) responseEntity.getBody()).isEqualTo(uc.getUserById(1));
	}
	
	@Test
	@Order(1)
	void testSaveUser() {
		when(urepo.save(userforTest)).thenReturn(userforTest);
		ResponseEntity<User> responseBySaveUser=new ResponseEntity<User>(userforTest,HttpStatus.CREATED);
		assertThat(uc.savee(userforTest)).isEqualTo(responseBySaveUser);

	}

	@Test
	@Order(5)
	void testDeleteUserById() throws ParseException {
		when(urepo.findById(1)).thenReturn(Optional.of(userforTest));
		ResponseEntity<Integer> responseBySaveUser=new ResponseEntity<Integer>(0,HttpStatus.ACCEPTED);
		assertThat(uc.deleteById(1)).isEqualTo(responseBySaveUser);
	}

	@Test
	@Order(3)
	void testUpdateUser() {
		ResponseEntity<Integer> responseBySaveUser=new ResponseEntity<Integer>(0,HttpStatus.OK);
		when(urepo.findById(1)).thenReturn(Optional.of(userforTest));
		userforTest.setName("Akshay");
		assertEquals(responseBySaveUser,uc.UpdateById(userforTest));
	}

	@Test
	@Order(4)
	void testLogInCheckOfUser() throws ParseException {
		User u=new User(1, "Lokesh", "Lokesh@gmail.com", "1212121212", "2121212121", GenderEnum.M, "02 JAN 2020",
				"lokesh");
		String encodedPwd=Base64.getEncoder().encodeToString(u.getPassword().getBytes());
		
		u.setPassword(encodedPwd);
		
		when(urepo.LoginCheck(u.getEmail_ID(), u.getPassword())).thenReturn(u);
		assertEquals(u, uc.getUserLogin(userforTest).getBody());

	}



}
