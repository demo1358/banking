package com.example.demo.entities1;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.example.demo.entities.Account;
import com.example.demo.entities.User;


public class AccountTest {
	
	  @Test
	    public void testSetBranchName() {
	    	Account account = new Account();
	    	account.setBranch_Name("pune");
	        assertTrue(account.getBranch_Name() == "pune");
	    }
	  
	  @Test
	    public void testSetAccountBalance() {
	    	Account account = new Account();
	    	account.setAccount_balance(2000.00);
	        assertTrue(account.getAccount_balance() == 2000.00);
	    }
	  
	  @Test
	    public void testSetAccountType() {
	    	Account account = new Account();
	    	account.setAccount_type('S');
	        assertTrue(account.getAccount_type() == 'S');
	    }
	  
	  @Test
	    public void testSetAccountID() {
	    	Account account = new Account();
	    	account.setAccount_id(101);
	        assertTrue(account.getAccount_id() == 101);
	    }
	  
	  //
	  
	  @Test
	    public void testGetBranchName() {
	    	Account account = new Account();
	    	account.setBranch_Name("pune");
	        assertTrue(account.getBranch_Name() == "pune");
	    }
	  
	  @Test
	    public void testGetAccountBalance() {
	    	Account account = new Account();
	    	account.setAccount_balance(2000.00);
	        assertTrue(account.getAccount_balance() == 2000.00);
	    }
	  
	  @Test
	    public void testGetAccountType() {
	    	Account account = new Account();
	    	account.setAccount_type('S');
	        assertTrue(account.getAccount_type() == 'S');
	    }
	  
	  @Test
	    public void testGetAccountID() {
	    	Account account = new Account();
	    	account.setAccount_id(101);
	        assertTrue(account.getAccount_id() == 101);
	    }
	  
	  

}
