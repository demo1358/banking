package com.example.demo.services1;


import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.constraints.Pattern;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import com.example.demo.entities.Account;
import com.example.demo.entities.User;
import com.example.demo.entities.User.GenderEnum;
import com.example.demo.repository.AccountRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.services.AccountService;
import com.example.demo.services.UserService;


@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
public class AccountServicesTest {
	@Autowired
	AccountService aservice;

	@Autowired
	UserService uservice;

	@MockBean
	AccountRepository arepo;

	@MockBean
	UserRepository urepo;

	private Account accountforTest;
	private Optional<Account> accountforTestOptional;
	private User userforTest;

	public AccountServicesTest() throws ParseException {
		accountforTest=new Account(101,"Pune",'S',2000.00);
		accountforTestOptional=Optional.of(new Account(101,"Pune",'S',2000.00));
		userforTest =new User(1, "Lokesh", "Lokesh@gmail.com", "1212121212", "2121212121", GenderEnum.M, "02 JAN 2020",
				"lokesh");
	}

	@Test
	@Order(2)
	void testGetAll() {
		List<Account> al=new ArrayList<>();
		al.add(accountforTest);
		when(arepo.findAll()).thenReturn(al);
		assertEquals(1, aservice.getAll().size());
	}

	@Test
	@Order(3)
	void testGetAccountById() {
		//when(arepo.findById(101)).thenReturn(accountforTestOptional);
		when(arepo.getAccountByAccountID(101)).thenReturn(accountforTest);
		assertEquals(accountforTest, aservice.getAccountByAccountID(101));
	}


	@Test
	@Order(1)
	void testAddAccount(){
		accountforTest.setUser_ID(userforTest);
		Optional<User> userforTestOptional=Optional.of(userforTest);
		when(urepo.findById(1)).thenReturn( userforTestOptional);
		assertEquals(userforTest, uservice.getUser(1));

		when(arepo.save(accountforTest)).thenReturn(accountforTest);
		assertThat(aservice.save(accountforTest)).isEqualTo(accountforTest);
	}

	@Test
	@Order(5)
	void testUpdateAccount() {
		accountforTest.setAccount_balance(2000.00);
		accountforTest.setUser_ID(userforTest);
		//long account_id,String branch_Name, char account_type, double account_balance, User user);
		when(arepo.updateAccountByID(101,"Pune",'S',2000.00,1)).thenReturn(1);
		assertEquals(1, aservice.updateAccountByID(accountforTest.getAccount_id(),accountforTest.getBranch_Name(),accountforTest.getAccount_type(),accountforTest.getAccount_balance(), accountforTest.getUser_ID().getUser_ID()));

	}

	@Test
	@Order(6)
	void testDeleteAccountById() {
		when(arepo.findById(101)).thenReturn(accountforTestOptional);
		assertEquals(accountforTest.getBranch_Name(),arepo.findById(101).get().getBranch_Name());
		aservice.DeleteByACCID(101);
		verify(arepo, times(1)).deleteACCById(101);
	}

	@Test
	@Order(4)
	void testGetAccountsByUserId() {
		when(arepo.getAccountByUserID(1)).thenReturn(Stream.of(new Account(101,
				"Pune",'S',2000.00)).collect(Collectors.toList()));
		assertEquals(1, aservice.getAccountByUserID(1).size());
	}
}

