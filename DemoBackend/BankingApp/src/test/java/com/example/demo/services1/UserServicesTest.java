package com.example.demo.services1;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.text.ParseException;
import java.util.Base64;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.example.demo.entities.User;
import com.example.demo.entities.User.GenderEnum;
import com.example.demo.repository.UserRepository;
import com.example.demo.services.UserService;



@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
public class UserServicesTest 
{

	@Autowired
	UserService uservice;

	@MockBean
	UserRepository urepo;

	private User userforTest;
	private Optional<User> userforTestOptional;
	
	public UserServicesTest() throws ParseException {

		userforTest =new User(1, "Lokesh", "Lokesh@gmail.com", "1212121212", "2121212121", GenderEnum.M, "02 JAN 2020",
				"lokesh");

		userforTestOptional=Optional.of(new User(1, "Lokesh", "Lokesh@gmail.com", "1212121212", "2121212121", GenderEnum.M, "02 JAN 2020",
				"lokesh"));
	}

	@Test
	@Order(1)
	void testSave() throws ParseException {

		when(urepo.save(userforTest)).thenReturn(userforTest);
		assertEquals(userforTest, uservice.save(userforTest));
	}

	@Test
	public void testGetUserById() throws ParseException {

		when(urepo.findById(1)).thenReturn(userforTestOptional);
		assertEquals(userforTest.getName(), uservice.getUser(1).getName());
	}

	@Test
	void testGetAll() throws ParseException
	{

		urepo.save(userforTest);
		when(urepo.findAll()).thenReturn(Stream.of(new User(1, "Lokesh", "Lokesh@gmail.com", "1212121212", "2121212121", GenderEnum.M, "02 JAN 2020",
				"lokesh")).collect(Collectors.toList()));
		assertEquals(1, uservice.getAll().size());
	}

	@Test
	public void testDeleteByID() throws ParseException {

		when(urepo.findById(1)).thenReturn(userforTestOptional);
		assertEquals(userforTestOptional, urepo.findById(1));
		when(urepo.deleteUserById(1)).thenReturn(1);
		//assertEquals(userforTest.getName(), urepo.findById(1).get().getName());
		uservice.deleteUser(1);
		assertEquals(1, uservice.deleteUser(1));
	    //verify(urepo,times(1)).deleteById(1);
		//verify(urepo,times(1)).deleteById(1);;
		//verify(uservice,times(1)).deleteUser(1);
	    
	    
//	    when(urepo.findById(1)).thenReturn(userforTestOptional);
//        assertEquals(userforTestOptional, urepo.findById(1));
//        uservice.DeleteByID(1);
//        verify(urepo,times(1)).deleteById(1);
	}
	
//	@Test
//	@Order(6)
//	void testDeleteAccountById() {
//		when(arepo.findById(101)).thenReturn(accountforTestOptional);
//		assertEquals(accountforTest.getBranch_Name(),arepo.findById(101).get().getBranch_Name() );
//		aservice.DeleteByACCID(101);
//		verify(arepo, times(1)).deleteACCById(101);
//	}


	@Test
	void testUpdateUser() throws ParseException {
		User uUpdate=new User(1, "Lokesh", "Lokesh@gmail.com", "1212121212", "2121212121", GenderEnum.M, "02 JAN 2020",
				"lokesh");

		when(urepo.findById(1)).thenReturn(userforTestOptional);
		assertEquals(userforTestOptional, urepo.findById(1));

		when(urepo.updateUserByID(1, uUpdate.getName(), uUpdate.getEmail_ID(), uUpdate.getMobile_number(),
				uUpdate.getSampleDate())).thenReturn(1);
		assertEquals(1, uservice.updateUserByID(1,uUpdate.getName(), uUpdate.getEmail_ID(), uUpdate.getMobile_number(),
				uUpdate.getSampleDate()));
	}

	@Test
	void testLogInCheck() throws ParseException {
		User u=new User(1, "Lokesh", "Lokesh@gmail.com", "1212121212", "2121212121", GenderEnum.M, "02 JAN 2020",
				"lokesh");
		String encodedPwd=Base64.getEncoder().encodeToString(u.getPassword().getBytes());
		u.setPassword(encodedPwd);

		when(urepo.LoginCheck(u.getEmail_ID(), u.getPassword())).thenReturn(u);
		assertEquals(u, uservice.getUserLog(userforTest.getEmail_ID(),Base64.getEncoder().encodeToString(userforTest.getPassword().getBytes())));

	}
}