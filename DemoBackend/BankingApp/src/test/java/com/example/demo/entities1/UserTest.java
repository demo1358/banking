package com.example.demo.entities1;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Date;
import java.text.SimpleDateFormat;

import org.junit.jupiter.api.Test;

import com.example.demo.entities.User;
import com.example.demo.entities.User.GenderEnum;

public class UserTest {
	
//	User user = new User();
	
//	Name = name;
//	Email_ID = email_ID;
//	Mobile_number = mobile_number;
//	Secondary_Mobile = secondary_Mobile; // yyyy-mm-dd
//	Gender = gender;
//	this.sampleDate = new Date(new SimpleDateFormat("dd MMM yyyy").parse(sampleDate).getDate());
//	this.Password = password;
	
    @Test
    public void testSetName() {
    	User user = new User();
    	user.setName("amol");
        assertTrue(user.getName() == "amol");
    }
    
    @Test
    public void testSetEmailID() {
    	User user = new User();
    	user.setEmail_ID("amol@gmail.com");
        assertTrue(user.getEmail_ID() == "amol@gmail.com");
    }
    
    @Test
    public void testSetMobileNumber() {
    	User user = new User();
    	user.setMobile_number("1212121212");
        assertTrue(user.getMobile_number() == "1212121212");
    }
    
    @Test
    public void testSetSecondaryMobile() {
    	User user = new User();
    	user.setSecondary_Mobile("2121212121");
        assertTrue(user.getSecondary_Mobile() == "2121212121");
    }
    
    @Test
    public void testSetPassword() {
    	User user = new User();
    	user.setPassword("amol@123");
        assertTrue(user.getPassword() == "amol@123");
    }
    
    @Test
    public void testSetGender() {
    	User user = new User();
    	user.setGender(GenderEnum.F);
        assertTrue(user.getGender() == GenderEnum.F);
    }
    
//    @Test
//    public void testSetSampleDate() {
//    	User user = new User();
//    	user.set("amol@123");
//        assertTrue(user.getPassword() == "amol@123");
//    }
    
    @Test
    public void testGetName() {
    	User user = new User();
    	user.setName("amol");
        assertTrue(user.getName() == "amol");
    }
    
    @Test
    public void testGetEmailID() {
    	User user = new User();
    	user.setEmail_ID("amol@gmail.com");
        assertTrue(user.getEmail_ID() == "amol@gmail.com");
    }
    
    @Test
    public void testGetMobileNumber() {
    	User user = new User();
    	user.setMobile_number("1212121212");
        assertTrue(user.getMobile_number() == "1212121212");
    }
    
    @Test
    public void testGetSecondaryMobile() {
    	User user = new User();
    	user.setSecondary_Mobile("2121212121");
        assertTrue(user.getSecondary_Mobile() == "2121212121");
    }
    
    @Test
    public void testGetPassword() {
    	User user = new User();
    	user.setPassword("amol@123");
        assertTrue(user.getPassword() == "amol@123");
    }
    
    @Test
    public void testGetGender() {
    	User user = new User();
    	user.setGender(GenderEnum.F);
        assertTrue(user.getGender() == GenderEnum.F);
    }
    
 


	
}
