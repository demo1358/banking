package com.example.demo.advice;

import java.util.NoSuchElementException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import com.example.demo.exception.CustomException;

@ControllerAdvice
public class BankControllerAdvice extends ResponseEntityExceptionHandler{
	
		@ExceptionHandler(NoSuchElementException.class)
		public ResponseEntity<String> handleNoSuchElementException(NoSuchElementException noSuchElementException)
		{
			return new ResponseEntity<String>("Invalid ID!! Element Not Found!!", HttpStatus.NOT_FOUND);
		}
	
		@ExceptionHandler(CustomException.class)
		public ResponseEntity<String> handleCommonException(CustomException commonException)
		{
			return new ResponseEntity<String>(commonException.getErrorMessage(), commonException.getErrorCode());
		}
		
		@Override
		protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
				HttpHeaders headers, HttpStatus status, WebRequest request) {
			return new ResponseEntity<Object>("Invalid HTTP Request Method Type!!", HttpStatus.NOT_FOUND);
		}
				

}
