package com.example.demo.entities;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.example.demo.entities.User.GenderEnum;

@Entity
@Table(name = "account")
public class Account {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int Account_id;

	@Column
	@Size(min = 3, max = 40, message = "Branch Name must contain min 3 char and max 40 char")
	private String Branch_Name;

	@Column
	private char Account_type;


	@Column
	@Positive(message = "Account Balance cannot be negative")
	private double Account_balance;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	// @JoinColumn(referencedColumnName = "user_ID")
	private User User_ID;

	public Account() {
		super();
	}

	public Account(int account_id,
			@Size(min = 3, max = 40, message = "Branch Name must be min 3 char max 40 char") String branch_Name,
			char account_type, @Positive(message = "Balance cannot be negative") double account_balance, User user_ID) {
		super();
		Account_id = account_id;
		Branch_Name = branch_Name;
		this.Account_type = account_type;
		Account_balance = account_balance;
		User_ID = user_ID;
	}


	public Account(@Size(min = 3, max = 40, message = "Branch Name must be min 3 char max 40 char") String branch_Name,
			char account_type, @Positive(message = "Balance cannot be negative") double account_balance) {
		super();
		Branch_Name = branch_Name;
		this.Account_type = account_type;
		Account_balance = account_balance;
	}
	
	

	public Account(int account_id,
			@Size(min = 3, max = 40, message = "Branch Name must contain min 3 char and max 40 char") String branch_Name,
			char account_type, @Positive(message = "Account Balance cannot be negative") double account_balance) {
		super();
		Account_id = account_id;
		Branch_Name = branch_Name;
		this.Account_type = account_type;
		Account_balance = account_balance;
	}

	public Account(String branch_Name, char account_type, double account_balance, User user_ID1) {
		super();
		Branch_Name = branch_Name;
		//this.Account_type = Account_type.valueOf(account_type);
		this.Account_type = account_type;
		Account_balance = account_balance;
		User_ID = user_ID1;
	}

	public int getAccount_id() {
		return Account_id;
	}

	public void setAccount_id(int account_id) {
		Account_id = account_id;
	}

	public String getBranch_Name() {
		return Branch_Name;
	}

	public void setBranch_Name(String branch_Name) {
		Branch_Name = branch_Name;
	}

	public char getAccount_type() {
		return Account_type;
	}

	public void setAccount_type(char account_type) {
		Account_type = account_type;
	}

	public double getAccount_balance() {
		return Account_balance;
	}

	public void setAccount_balance(double account_balance) {
		Account_balance = account_balance;
	}

	public User getUser_ID() {
		return User_ID;
	}

	public void setUser_ID(User user_ID) {
		User_ID = user_ID;
	}

	@Override
	public String toString() {
		return "Account [Account_id=" + Account_id + ", Branch_Name=" + Branch_Name + ", Account_type=" + Account_type
				+ ", Account_balance=" + Account_balance + ", User_ID=" + User_ID + "]";
	}

}
