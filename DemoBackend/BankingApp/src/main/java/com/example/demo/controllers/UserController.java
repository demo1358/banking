package com.example.demo.controllers;

import java.util.Date;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.User;
import com.example.demo.services.UserService;

@RestController
@CrossOrigin("*")
public class UserController {
	@Autowired
	UserService userService;

	Encoder encoder = Base64.getEncoder();

	@GetMapping("/allUsers")
	public ResponseEntity<List<User>> fetch() {
		List<User> listOfAllUsers = userService.getAll();
		return new ResponseEntity<List<User>>(listOfAllUsers, HttpStatus.OK);
	}

	@PostMapping("/login")
	public ResponseEntity<User> getUserLogin(@RequestBody User u) {
		String encodedPass = encoder.encodeToString(u.getPassword().getBytes());
		u.setPassword(encodedPass);
		System.out.println(u.getEmail_ID() + " " + u.getPassword());
		User user = userService.getUserLog(u.getEmail_ID(), u.getPassword());
		System.out.println(user);
		return new ResponseEntity<User>(user, HttpStatus.OK);


	}

	@GetMapping("/getUserByID/{id}")
	public ResponseEntity<User> getUserById(@PathVariable int id) {
		User userFound = userService.getUser(id);
		return new ResponseEntity<User>(userFound, HttpStatus.OK);
	}

	@PostMapping("/addUser")
	public ResponseEntity<User> savee(@RequestBody User u) {
		// System.out.println(u.getDOB());
		System.out.println(u.getSampleDate());

		String EncodedPass = encoder.encodeToString(u.getPassword().getBytes());
		u.setPassword(EncodedPass);
		System.out.println(u);
		User userSave = userService.save(u);
		return new ResponseEntity<User>(userSave, HttpStatus.CREATED);
		// return "heeeeyy";
	}

	@DeleteMapping("/deleteUser/{id}")
	public ResponseEntity<Integer> deleteById(@PathVariable int id) {
		System.out.println(id);
		//userService.deleteUser(id);
		return new ResponseEntity<Integer>(userService.deleteUser(id), HttpStatus.ACCEPTED);
	}

	@PostMapping("/updateUser")
	public ResponseEntity<Integer> UpdateById(@RequestBody User u) {
		System.out.println(u.getSampleDate());
		System.out.println(u);
		int i=userService.updateUserByID(u.getUser_ID(), u.getName(), u.getEmail_ID(), u.getMobile_number(),
				u.getSampleDate());
		return new ResponseEntity<Integer>(i, HttpStatus.OK);
	}

}
