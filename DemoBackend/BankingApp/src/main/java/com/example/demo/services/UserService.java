package com.example.demo.services;

import java.sql.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.example.demo.entities.User;
import com.example.demo.exception.CustomException;
import com.example.demo.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	private UserRepository urepo;

	public User save(User u) {
		if (u.getName().isEmpty() || u.getEmail_ID().isEmpty() || u.getGender() == null || u.getPassword().isEmpty()
				|| u.getMobile_number().isEmpty() || u.getSampleDate() == null)
			throw new CustomException(HttpStatus.BAD_REQUEST, "Input Field is Empty !!!");
		else if (!u.getEmail_ID().contains("@"))
			throw new CustomException(HttpStatus.BAD_REQUEST, "Email ID is invalid !!!");
		else if (u.getMobile_number().length() != 10)
			throw new CustomException(HttpStatus.BAD_REQUEST, "Mobile Number needs to be of length 10 !!!");
//		else if ((!(u.getGender().toString().equals("F")) && (!(u.getGender().toString().equals("M")))))
//			throw new CustomException(HttpStatus.BAD_REQUEST, "Gender should be either M or F !!!");
		//t or false
		// ConstraintViolationException
		return urepo.save(u);
	}

	public User getUser(int User_ID) {
		return urepo.findById(User_ID).get();
	}

	public List<User> getAll() {
		if (urepo.findAll().isEmpty()) {
			throw new CustomException(HttpStatus.NOT_FOUND, "List is Empty!!!");
		}

		return urepo.findAll();
	}

	public int deleteUser(int User_ID) {
		User u = urepo.findById(User_ID).get();
		if (u==null)
			throw new NoSuchElementException();
		else
		return urepo.deleteUserById(User_ID);
	}

	public int updateUserByID(int User_ID, String Name, String Email_ID, String Mobile_number, Date date) {
		if (User_ID == 0 || Name.isEmpty() || Email_ID.isEmpty() || date.equals(null) || Mobile_number.isEmpty())
			throw new CustomException(HttpStatus.NOT_FOUND, "Input field is empty !!");

		return urepo.updateUserByID(User_ID, Name, Email_ID, Mobile_number, date);
	}

	public User getUserLog(String Email_ID, String Password) {
		if (Email_ID.isEmpty() || Password.isEmpty())
			throw new CustomException(HttpStatus.NOT_FOUND, "Input Field is empty !!");

		return urepo.LoginCheck(Email_ID, Password);
	}

}
