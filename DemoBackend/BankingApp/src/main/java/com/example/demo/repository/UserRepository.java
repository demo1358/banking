package com.example.demo.repository;



import java.sql.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entities.User;

@Transactional
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	
	@Modifying
	@Query(value=" delete from user where User_ID=?1",nativeQuery = true)
	public int deleteUserById(int User_ID);
	
	@Modifying
	@Query(value=" update user where User_ID=?1",nativeQuery = true)
	public int updateUserById(int User_ID);

	@Modifying
	@Query(value=" update user set Name=?2,Email_ID=?3,Mobile_number=?4,sample_date=?5 where User_ID=?1 ",nativeQuery = true)
	public int updateUserByID(int User_ID,String Name,String Email_ID,String mobile_number,Date DOB);
	
	@Query(value=" select * from user where email_id=?1 && password=?2",nativeQuery = true)
	public User LoginCheck(String Email_ID, String Password);
	
}
